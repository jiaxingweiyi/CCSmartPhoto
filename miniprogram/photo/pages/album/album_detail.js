const ccminiCloudHelper = require('../../helper/ccmini_cloud_helper.js');
const ccminiHelper = require('../../helper/ccmini_helper.js');
const ccminiBizHelper = require('../../helper/ccmini_biz_helper.js');
const ccminiPageHelper = require('../../helper/ccmini_page_helper.js');
const PassportBiz = require('../../biz/passport_biz.js');


Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		isLoad: false,
		isEdit: false,

		auto: true,
		current: 0,
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: async function (options) {
		await PassportBiz.initPage(this);

		if (!await PassportBiz.loginMustRegWin(this)) return;
		if (!ccminiPageHelper.getId(this, options)) return;

		this._loadDetail();
	},

	_loadDetail: async function () {
		let id = this.data.id;
		if (!id) return;

		let params = {
			id,
		};
		let opt = {
			hint: false
		};
		let album = await ccminiCloudHelper.callCloudData('album/view', params, opt);
		if (!album) return;

		this.setData({
			isLoad: true,
			album,
			isEdit: (album.ALBUM_USER_ID === PassportBiz.getUserId()),
		});


	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: async function () {
		await this._loadDetail();
		wx.stopPullDownRefresh();
	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {

	},

	url: function (e) {
		ccminiPageHelper.url(e);
	},

	onPageScroll: function (e) {
		// 回页首按钮
		ccminiPageHelper.showTopBtn(e, this);

	},

	/**
	 * 编辑 
	 */
	myEditListener: function (e) {
		ccminiPageHelper.goto('album_edit?id=' + this.data.id, 'redirect');
	},

	/**
	 * 删除 
	 */
	myDelListener: async function (e) {
		if (!await PassportBiz.loginMustRegWin(this)) return;

		let id = this.data.id;
		let callback = async function () {
			await ccminiCloudHelper.callCloudSumbit('album/del', {
				id
			}).then(res => {
				ccminiPageHelper.delPrevPageListNode(id);
				ccminiPageHelper.showSuccToast('删除成功', 1500, function () {
					ccminiPageHelper.goto('album_index', 'redirect');
				})
			}).catch(err => {});
		}

		ccminiPageHelper.showConfirm('您确认删除？删除后不可恢复', callback);
	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function (res) {
		return {
			title: this.data.album.ALBUM_TITLE,
			path: '/pages/album/album_detail?id=' + this.data.id,
		}
	},

	bindAlbumChange: function (e) {
		this.setData({
			current: e.detail.current
		})
	},
	catchPrevImgTap: function () {
		let current = this.data.current;
		current = current < (this.data.album.ALBUM_PIC.length - 1) ? current + 1 : 0;
		this.setData({
			current,
		})
	},

	catchAutoTap: function () {
		this.setData({
			auto: !this.data.auto
		});
	},

	catchNextImgTap: function () {
		let current = this.data.current;
		current = current > 0 ? current - 1 : this.data.album.ALBUM_PIC.length - 1;
		this.setData({
			current,
		})
	},

})