# 技术交流 
- 开发交流，技术分享，问题答疑，功能建议收集，版本更新通知！

 ![输入图片说明](cc.png)




# 项目介绍


- 本小程序采用腾讯小程序云开发技术，不需要单独部署服务器和服务端程序
- 功能模块包括相册用户注册，相册列表，相册分类，相册发布，相册分享，后台管理等6大功能模块！ 

# UI设计思路
 ![输入图片说明](https://images.gitee.com/uploads/images/2021/0819/173820_e626c51a_9240987.png "微信图片_20210819173717.png")

# 功能说明
 ![输入图片说明](https://images.gitee.com/uploads/images/2021/0819/173831_e93f0ba7_9240987.png "func导图1 (2).png")

# 技术运用

- 项目使用微信小程序平台进行开发。
- 使用腾讯云开发技术，免费资源配额，无需域名和服务器即可搭建。
- 小程序本身的即用即走，适合小工具的使用场景，也适合程序的开发。

# 项目效果截图
 ![输入图片说明](https://images.gitee.com/uploads/images/2021/0819/173852_14ee53f2_9240987.png "相册列表.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0819/173904_749b8d94_9240987.png "相册详细.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0819/173915_21a8f47a_9240987.png "创建相册.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0819/173923_eaa92d04_9240987.png "个人中心.png")

# 项目后台截图
 ![输入图片说明](https://images.gitee.com/uploads/images/2021/0819/173931_992d8fe4_9240987.png "后台登录.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0819/173940_ac5a8572_9240987.png "后台管理首页.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0819/173949_39cb6013_9240987.png "后台相册管理.png")

# 部署教程：
### 0. 了解小程序云开发的基础知识
-  参考微信小程序官方文档：
- https://developers.weixin.qq.com/miniprogram/dev/wxcloud/basis/getting-started.html
- https://developers.weixin.qq.com/miniprogram/dev/wxcloud/quick-start/miniprogram.html

### 1 源码导入微信开发者工具
  
  ![输入图片说明](https://images.gitee.com/uploads/images/2021/0819/174330_6cb06b72_9240987.png "创建.png")

 

### 2 开通云开发环境
 -  参考微信官方文档：https://developers.weixin.qq.com/miniprogram/dev/wxcloud/basis/getting-started.html
- 在使用云开发能力之前，需要先开通云开发。 
- 在开发者工具的工具栏左侧，点击 “云开发” 按钮即可打开云控制台，根据提示开通云开发，并且创建一个新的云开发环境。
![输入图片说明](https://images.gitee.com/uploads/images/2021/0811/232537_8a27b61c_9240987.png "云开发开通环境.png")
- 每个环境相互隔离，拥有唯一的环境 ID(拷贝此ID，后面配置用到)，包含独立的数据库实例、存储空间、云函数配置等资源；
 

#### 3 云函数及配置
- 本项目使用到了一个云函数photo_cloud 


- 在云函数cloudfunctions文件夹下选择云函数photo_cloud  , 右键选择在终端中打开,然后执行 
- npm install –product
![输入图片说明](https://images.gitee.com/uploads/images/2021/0819/174028_0130fc9b_9240987.png "安装云函数依赖0.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0819/174042_f9328a0e_9240987.png "安装云函数依赖.png")

-  **如提示npm无法识别，请先恶补npm和node.js基础知识：https://www.runoob.com/nodejs/nodejs-npm.html** 


 

- 打开cloudfunctions/sport_cloud/comm/ccmini_config.js文件，配置后台管理员账号和密码

 ![输入图片说明](https://images.gitee.com/uploads/images/2021/0911/150146_a9af88e5_9240987.png "设置管理员账号.png")

 


#### 4  客户端配置
- 打开miniprogram/app.js文件，配置环境ID

 ![输入图片说明](https://images.gitee.com/uploads/images/2021/0811/232832_6053aae0_9240987.png "客户端配置.png")


#### 5  云函数配置
- 在微信开发者工具-》云开发-》云函数-》对指定的函数添加环境变量 
- [服务端时间时区TZ] =>Asia/Shanghai
- [函数内存] =>128M   
- [函数超时时间] => 20秒
 ![输入图片说明](https://images.gitee.com/uploads/images/2021/0819/174054_cfd5b34b_9240987.png "云函数配置.png")

 

#### 6  设置图片域名信任关系
- 进入小程序 开发管理=》开发设置=》服务器域名 =》downloadFile合法域名	
- 添加2个域名：
- 1）你的云存储域名，格式类似：https://1234-test-pi5po-1250248.tcb.qcloud.la
- 2）微信头像域名：https://thirdwx.qlogo.cn 
![输入图片说明](https://images.gitee.com/uploads/images/2021/0811/233716_fccfac0e_9240987.png "业务域名.png")

#### 7  上传云函数&指定云环境ID
 ![输入图片说明](https://images.gitee.com/uploads/images/2021/0819/174120_a089d92c_9240987.png "云函数上传.png")

### 至此完全部署配置完毕。

### 在线演示：
 

 ![输入图片说明](https://images.gitee.com/uploads/images/2021/0811/233918_96b29222_9240987.jpeg "Free版-QR.jpg")


### 如有疑问，欢迎骚扰联系我鸭： 
### 俺的微信:  cclinux0730


